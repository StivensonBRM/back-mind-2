<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    $client = new Client([
        // Base URI is used with relative requests
        'base_uri' => 'http://10.130.210.21:81/api/user',
        // You can set any number of default request options.
        'timeout' => 5.0,

    ]);


    try {
        $response = $client->request('GET', '',
            [
                'headers' => [
                    'Authorization' => 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImZhNTdjNDVmZTJmZGU4NWYyOGIxZmExZjkyM2RhODUwOTIyZmFiZTY0NmY0ZDA0NWY4Yjk5YzVkYzg0MjE0MWU1YWY2YTc2MzBhZGViYjIwIn0.eyJhdWQiOiIxIiwianRpIjoiZmE1N2M0NWZlMmZkZTg1ZjI4YjFmYTFmOTIzZGE4NTA5MjJmYWJlNjQ2ZjRkMDQ1ZjhiOTljNWRjODQyMTQxZTVhZjZhNzYzMGFkZWJiMjAiLCJpYXQiOjE1NTEyODcxMzAsIm5iZiI6MTU1MTI4NzEzMCwiZXhwIjoxNTgyODIzMTI5LCJzdWIiOiI0MCIsInNjb3BlcyI6W119.WyiwEwPkyprSFSUaAi845k2mMvbfF9GSVPQEkKRjay97HAFB7numDGgUxhQrugPZB63PtHpieKhrvfoVATmfdTq_8AtH--1qPqiNxK3Ux73KD5Y4_5fvAgPBFdkjEhe_qsIAPWMcmTo2xS5yKrfDGroZKYQ5H4PK3ql4-vYDzkTMD39W4oCV56Fir1ms__YYh3Ft6xHZp0YaCi-_bmPNbdSUnj1c5Nr19tnZUWWmtgV1fLPw9A1EJzLTKEift8WdJitVnMGl-3H2-zHmlBExkUeSqIiRymCV5SUqUVL43uE8vD_qxyTT1FwsvPBFXwihTyeaq6j0zRG1Pqgp62hJzy39nL8fN9QDGaqFaEAMctaWVmkRp3wI0_cfWG_win70MMaYh6qWLn7YdiSqKELha3RvcWncXZ4qlcOTvMHiu2row4WgOf9btUZz7PwgsQNYfBLi5Z60p7easvnbBnptGUgMXSl1LITExNGT53yyndGKFg478Yzm-7Oss8jCgItdbOKzb9Dt-7RdMh5ZmqqIl07mNi7zHRr6fSEfbRhpffdrNX5ZFy0MATavJGR6dv6Yzf5lKCWDGdXZWTrFEWCB3MBir6r3mf6um0Utrhp1rArrLLD0zH9-n__oR_jWJ-ox-vqRnCuM7cBiMjJ3OOnZgxH2M_wJSAwXkZuvSIuUnHI',
                    'Accept' => 'application/json',

                ]
            ]
        )->getBody()->getContents();
        return $response;
    } catch (RequestException $e) {
        return "ERROR PERRO ";
    }

    dd($response);

});


